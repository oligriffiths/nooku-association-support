<?php
/**
 * User: Oli Griffiths
 * Date: 02/07/2012
 * Time: 19:11
 */

class KDatabaseBehaviorAssociatable extends KDatabaseBehaviorAbstract
{
	/**
	 * Association information.
	 * Associations must be specified in the following format:
	 *
	 *
	 *  $associations = array(
	 *      property_name => array(
	 *          'type' => (string) one_one|one_many|many_many
	 *           'model' => 'a nooku model identifier string or object',
	 *              this is the identifer for the source model where the data is being retrieved from
	 *
	 *          'keys' => array('local_key' => 'foreign_key'),
	 *              local key is the column in the source/through table that contains the value, foreign key is the key for the associated table.
	 *              when creating a many to many association this refers to the keys from the through table to the target table
	 *
	 *          'through' => 'a nooku model identifier' (optional)
	 *              only required for many to many association. This is the ID for the association table
	 *
	 *          'through' => array('local_key' => 'foreign_key')
	 *              local key is the column in the source table that contains the value, foreign key is the column in the through table
	 *      )
	 * )
	 *
	 * @var array
	 */
	static protected $_tables;
	static protected $_table_relations;
	static protected $_associations_db = array();

	/**
	 * Holds the associations for the mixed table
	 * @var
	 */
	protected $_associations;

	/**
	 * Stores associations passed in the config
	 * @var array
	 */
	protected $_associations_config;

	/**
	 * Stores alias info for associations
	 * @var
	 */
	protected $_associations_aliases;

    /**
     * Holds the associated data
     * @var array
     */
    protected $_associated = array();

	/**
	 * Holds the table object
	 * @var KDatabaseTableAbstract
	 */
	protected $_table;

	/**
	 * Set to true if one to many deletions should happen.
	 * This should really be applied at DB level, but is here if this is not possible (MyIsam);
	 * @var array false by default
	 */
	protected $_delete_support;


	/**
	 * Response object
	 * @var
	 */
	protected $_response;


	/**
	 * Constructor.
	 * Sets up the associations
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 */
	public function __construct(KConfig $config)
	{
		parent::__construct($config);

		//Set the table
		$this->_table = $config->mixer;

		//Associations can be set through the config
		$this->_associations_config = $config->associations->toArray();

		//Store association aliases
		$this->_associations_aliases = $config->aliases->toArray();

		//Gather the defined associations from the row
		if(isset($this->_table->associations)){
			$this->_associations_config = array_merge((array) $this->_table->associations, $this->_associations_config);
		}

		//Set one_many deletion flag
		$this->_delete_support = $config->delete_support;

		//Store the response object for afterUpdate/delete
		$this->_response = version_compare(Koowa::VERSION, '12.3', 'ge') ? $this->getService('koowa:controller.response.default') : null;
	}


	/**
	 * @param KConfig $config
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_HIGHEST,
			'auto_mixin' => true,
			'associations' => array(),
			'aliases' => array(),
			'delete_support' => array()
		));

		parent::_initialize($config);
	}


	/**
	 * Instantiate the object
	 *
	 * If the behavior is auto mixed also lazy mix it into related row objects.
	 *
	 * @param     object     An optional KConfig object with configuration options
	 * @param     object    A KServiceInterface object
	 * @return  object
	 */
	public static function getInstance(KConfigInterface $config, KServiceInterface $container)
	{
		$instance  = parent::getInstance($config, $container);

		//If the behavior is auto mixed also lazy mix it into related row objects.
		if ($config->auto_mixin && version_compare(Koowa::VERSION, '12.2', 'le'))
		{
			$identifier = clone $instance->getMixer()->getIdentifier();
			$identifier->path = array('database', 'row');
			$identifier->name = KInflector::singularize($identifier->name);

			$container->addMixin($identifier, $instance);
		}

		return $instance;
	}


	/**
	 * Creates get$property methods for all associations
	 * @param KObject $mixer
	 * @return array
	 */
	public function getMixableMethods(KObject $mixer = null)
	{
		$methods = parent::getMixableMethods($mixer);

		if($mixer instanceof KDatabaseRowInterface){
			$this->detectAssociations();
			$associations   = $this->getAssociations();

			foreach(array_keys($associations) AS $property){
				$methods['get'.ucfirst($property)] = version_compare(Koowa::VERSION,'12.3','ge') ? $this : 'get'.ucfirst($property);
				$methods[$property] = version_compare(Koowa::VERSION,'12.3','ge') ? $this : $property;
			}
		}

		return $methods;
	}


	/**
	 * Auto mixin on select
	 * @param KCommandContext $context
	 */
	protected function _afterTableSelect(KCommandContext $context)
	{
		if(version_compare(Koowa::VERSION,'12.2','le')){
			if($context->data instanceof KDatabaseRowAbstract) $context->data->isAssociatable();
			if($context->data instanceof KDatabaseRowsetAbstract) foreach($context->data AS $row) $row->isAssociatable();
		}
	}


	/**
	 * Save all associated data. Associated data may be an array or a kconfig object.
	 * The data then has the appropriate keys set for the association and the data is passed to the controller and saved
	 * If the controller returns an error, this is set as the status in the initiating row
	 * @param KCommandContext $context
	 */
	protected function _afterTableUpdate(KCommandContext $context)
	{
		if($context->data instanceof KDatabaseRowAbstract && $context->data->isAssociatable()){
			$data = $context->data->getData();
			$hash = spl_object_hash($context->data);

			foreach($this->getAssociations() AS $association_id => $association){

				if(!isset($data[$association_id])) continue;

				$value = $data[$association_id];
				if(is_array($value) || $value instanceof KConfig){

					$association    = new KConfig($association);
					$keys           = KConfig::unbox($association->keys);
					$identifier     = clone $this->getIdentifier($association->model);
					$identifier->path = 'controller';
					$identifier->name = KInflector::singularize($identifier->name);

					$success = true;
					$error = null;

					try{
						switch($association->type){
							case 'one_one':
								//Set the related keys
								foreach($keys AS $lk => $fk) $value[$fk] = $context->data->$lk;

								$controller     = $this->getService($identifier);
								$new_context    = $controller->getCommandContext();
								$new_context->response = $this->_response;

								//Ensure all model states are set
								$model = $controller->getModel();
								foreach($value AS $k => $v){
									$model->$k = $v;
								}

								//Set the new context data
								$new_context->data = $value;

								//If action flag is set, run delete action
								if(isset($value['action'])){
									$action = $value['action'];
								}else{
									$action = 'save';
								}

								//Run the action
								$result = $controller->$action($new_context);

								//Check for errors
								if(version_compare(Koowa::VERSION, '12.2','le')){
									if($new_context->getError()){
										$success = false;
										$error = $new_context->getError();
									}else{
										$context->affected = true;
									}
								}else{
									if ($new_context->response->isError())
									{
										$success = false;
										$error = $new_context->response->getStatusMessage();
									}else{
										$context->affected = true;
									}
								}

								//Update the the mixer with the id of the result in-case a new record has been created
								if($context->affected){
									foreach($keys AS $lk => $fk) $this->_mixer->$lk = $result->$fk;
									$this->getTable()->getCommandChain()->disable();
									$result->save();
									$this->getTable()->getCommandChain()->enable();

									//Cache the result
									$this->_associated[$hash][$association_id] = $result;
								}

								break;

							case 'one_many':
								$values = $value;

								foreach($values AS $k => $value){

									$controller     = $this->getService($identifier);
									$new_context    = $controller->getCommandContext();
									$new_context->response = $this->_response;

									//Set the associated property
									$new_context->associated = $context->data;

									//Set pk as index from values
									if(!is_numeric($k) || (is_numeric($k) && $k > 0)) $value['id'] = $k;

									//Set local keys in the related data
									foreach($keys AS $lk => $fk){
										$value[$fk] = $context->data->$lk;
									}

									//Ensure all model states are set
									$model = $controller->getModel();
									foreach($value AS $k => $v){
										$model->$k = $v;
									}

									//Set the new context data
									$new_context->data = $value;

									//If action flag is set, run delete action
									if(isset($value['action'])){
										$action = $value['action'];
									}else{
										$action = 'save';
									}

									//Run the action
									$controller->$action($new_context);

									//Check for errors
									if(version_compare(Koowa::VERSION, '12.2','le')){
										if($new_context->getError()){
											$success = false;
											$error = $new_context->getError();
										}else{
											$new_context->affected = true;
										}
									}else{
										if ($new_context->response->isError())
										{
											$success = false;
											$error = $new_context->response->getStatusMessage();
										}else{
											$context->affected = true;
										}
									}
								}

								//Clear cached result
								if($context->affected){
									unset($this->_associated[$hash][$association_id]);
								}

								break;

							//Many to many relationships only save in the "through" table
							case 'many_many':
								$values = $value;

								//Get the through table identifier
								$identifier         = clone $this->getIdentifier($association->through);
								$identifier->path   = 'controller';
								$identifier->name   = KInflector::singularize($identifier->name);
								$controller         = $this->getService($identifier);
								$new_context        = $controller->getCommandContext();
								$new_context->response = $this->_response;
								$fk_property        = KInflector::singularize($association_id).'_id';
								$through_keys       = KConfig::unbox($association->through_keys);

								//Set the associated property
								$new_context->associated = $context->data;

								//Set the controller request key to a state is d
								$model = $controller->getModel();
								foreach($through_keys AS $lk => $fk){
									$controller->$fk = $context->data->$lk;
									$model->$fk = $context->data->$lk;
								}

								//Get the existing records from the through table
								$existing = $controller->getModel()->getList();
								$remove = array();
								foreach($existing AS $row){
									//If the index does not exist (it's being removed), add to the remove array
									//else unset the value so it's not re-saved
									foreach(array_keys($keys) AS $key){
										$index = array_search($row->$key, $values);
										if($index === false) $remove[$key] = $row;
										else unset($values[$index]);
									}
								}

								//Delete the rows to be removed
								foreach($remove AS $key => $row){
									$controller->$key = $row->$key;
									$controller->delete($new_context);
								}

								//Add any new values
								foreach($values AS $value){

									//Set the controller state for new value
									$new_value = array();
									foreach($through_keys AS $lk => $fk){
										$controller->$fk = $context->data->$lk;
										$controller->$fk_property = $value;
										$new_value = array($fk => $context->data->$lk, $fk_property => $value);
									}

									//Set the new data
									$new_context->data = $new_value;

									//Save the row
									$controller->save($new_context);

									//Check for errors
									if(version_compare(Koowa::VERSION, '12.2','le')){
										if($new_context->getError()){
											$success = false;
											$error = $new_context->getError();
										}else{
											$context->affected = true;
										}
									}else{
										if ($new_context->response->isError())
										{
											$success = false;
											$error = $new_context->response->getStatusMessage();
										}else{
											$context->affected = true;
										}
									}
								}

								//Clear cached result
								if($context->affected){
									unset($this->_associated[$hash][$association_id]);
								}

								break;
						}
					}catch(KException $e){
						$success = false;
						$error = $e->getMessage();
					}

					//If saving associations failed, ensure we return the correct response
					if(!$success){
						$context->affected = false;
						$context->data->setStatusMessage($error);
						return false;
					}
				}
			}
		}
	}


	/**
	 * Run update to save associated data
	 * @param KCommandContext $context
	 */
	protected function _afterTableInsert(KCommandContext $context)
	{
		$this->_afterTableUpdate($context);
	}


	/**
	 * Removes associated data
	 * @param KCommandContext $context
	 */
	protected function _afterTableDelete(KCommandContext $context)
	{
		if(in_array('one_one',$this->_delete_support) || in_array('one_many',$this->_delete_support) || in_array('many_many',$this->_delete_support)){
			if($context->data instanceof KDatabaseRowAbstract && $context->data->isAssociatable()){

				foreach($this->getAssociations() AS $property => $association){

					$association        = new KConfig($association);
					$keys               = KConfig::unbox($association->keys);
					$identifier         = clone $this->getIdentifier($association->model);
					$identifier->path   = 'controller';
					$identifier->name   = KInflector::singularize($identifier->name);

					$success = true;
					$error = null;

					try{
						switch($association->type){
							case 'one_one':
							case 'one_many':
								if(in_array($association->type,$this->_delete_support)){
									$controller     = $this->getService($identifier);
									$new_context    = $controller->getCommandContext();
									$new_context->response = $this->_response;

									//Set the controller request keys
									$state = array();
									foreach($keys AS $lk => $fk){
										$state[$fk] = $context->data->$lk;
										$controller->$fk = $context->data->$lk;
									}

									//Get the models states
									$modelState = $controller->getModel()->getState()->toArray();

									//Ensure the state
									if(array_intersect_key($state, $modelState) != $state)
									{
										throw new KDatabaseBehaviorException('The model '.$controller->getModel()->getIdentifier().' must contain the following states for the associatable behavior to work: '.implode(',',array_keys($state)));
									}

									//Delete the rows
									$new_context->data = array();
									$data = $controller->getModel()->getList();
									if($data && $data->count()){
										$controller->delete($new_context);

										//Check for errors
										if(version_compare(Koowa::VERSION, '12.2','le')){
											if($new_context->getError()){
												$success = false;
												$error = $new_context->getError();
											}else{
												$context->affected = true;
											}
										}else{
											if ($new_context->response->isError())
											{
												$success = false;
												$error = $new_context->response->getStatusMessage();
											}else{
												$context->affected = true;
											}
										}
									}
								}

								break;

							//Many to many relationships only delete in the "through" table
							case 'many_many':
								if(in_array($association->type,$this->_delete_support)){
									//Get the through table identifier
									$identifier         = clone $this->getIdentifier($association->through);
									$identifier->path   = 'controller';
									$identifier->name   = KInflector::singularize($identifier->name);
									$controller         = $this->getService($identifier);
									$new_context        = $controller->getCommandContext();
									$new_context->response = $this->_response;
									$through_keys       = KConfig::unbox($association->through_keys);

									//Set the controller request keys
									$state = array();
									foreach($through_keys AS $lk => $fk){
										$state[$fk] = $context->data->$lk;
										$controller->$fk = $context->data->$lk;
									}

									//Get the models states
									$modelState = $controller->getModel()->getState()->toArray();

									//Ensure the state
									if(array_intersect_key($state, $modelState) != $state)
									{
										throw new KDatabaseBehaviorException('The model '.$controller->getModel()->getIdentifier().' must contain the following states for the associatable behavior to work: '.implode(',',array_keys($state)));
									}

									//Delete the rows
									$new_context->data = array();
									$data = $controller->getModel()->getList();
									if($data && $data->count()){
										$controller->delete($new_context);

										//Check for errors
										if(version_compare(Koowa::VERSION, '12.2','le')){
											if($new_context->getError()){
												$success = false;
												$error = $new_context->getError();
											}else{
												$context->affected = true;
											}
										}else{
											if ($new_context->response->isError())
											{
												$success = false;
												$error = $new_context->response->getStatusMessage();
											}else{
												$context->affected = true;
											}
										}
									}
								}

								break;
						}
					}catch(KException $e){
						$success = false;
						$error = $e->getMessage();
					}

					//If saving associations failed, ensure we return the correct response
					if(!$success){
						$context->affected = false;
						$context->data->setStatusMessage($error);
					}
				}
			}
		}
	}


	/**
	 * Sets the mixer object and loads the associations from the DB
	 * @param $mixer
	 * @return KDatabaseBehaviorAssociatable|KMixinInterface
	 */
	public function setMixer($mixer)
	{
		parent::setMixer($mixer);

		if($mixer instanceof KDatabaseRowAbstract && $this->_associations === null)
		{
            $hash = spl_object_hash($mixer);
            $this->_associated[$hash] = array();
			$this->setManyToManyData();
		}
		return $this;
	}


	/**
	 * Create the associations between the source table and other tables
	 * Associations are determined using the following rules.
	 *
	 * One to one associations:
	 * These are determined using the columns from this table. Any column ending _id is created as a association minus the _id
	 *
	 * One to many associations:
	 * These are determined following a naming convention. Any tables in the DB that belong to the same package that match the
	 * singular of the source table followed by an underscore. Also any tables that contain a column of the source table singular followed by _id.
	 * E.g. package_users is associated to package_user_groups. Also, package_users is associated to package_posts if posts contains a user_id column

	 * Many to many associations:
	 * These are determined following a naming convention. Any tables in the DB that belong to the same package that match the plural of the source table
	 * followed by or preceded by an underscore, eg posts_categories or categories_posts are a potential match. Also any tables that contain a column of the source table singular followed by _id are matched.
	 * If an association is defined for the table matched above, then an identifier for the source model is created by removing the plural suffix.
	 * E.g. package_posts is associated to package_posts_categories which is in turn associated to package_categories. So package_posts > package_posts_categories > package_categories
	 *
	 * @return mixed
	 */
	public function detectAssociations()
	{
		$table_name = $this->_table->getName();

		//Return if we've done this before
		if($this->_associations !== null) return $this->_associations;

		//Check APC cache for associations
		if(extension_loaded('apc')){
			if($associations = apc_fetch('koowa-cache-identifier-'.((string) $this->_table->getIdentifier()).'.associations'))
			{
				self::$_associations_db[$table_name] = $associations;
			}
		}

		//If associations not defined for this table, retrieve from DB schema
		if(!isset(self::$_associations_db[$table_name]))
		{
			$associations       = $this->_detectAssociationsByDB();
			$associations       = $this->_detectAssociationsByName($associations);

			//Store associations in apc
			if(extension_loaded('apc')){
				apc_store('koowa-cache-identifier-'.((string) $this->_table->getIdentifier()).'.associations', $associations);
			}

			//Store the associations
			self::$_associations_db[$table_name] = $associations;
		}

		//Store the associations
		$this->_associations = array_merge(self::$_associations_db[$table_name], $this->_associations_config);

		return $this->_associations;
	}


	/**
	 * Detects associations based on InnoDB relationships
	 * @param $table_name
	 * @return array
	 */
	protected function _detectAssociationsByDB($associations = array())
	{
		$table_name         = $this->_table->getName();
		$identifier         = clone $this->getMixer()->getIdentifier();
		$identifier->path   = array('model');

		/**
		 * Detect relations based on InnoDB relations
		 **/
		$innodb = $this->getTableRelations();
		if(isset($innodb[$table_name])){

			$relations = $innodb[$table_name];

			//Setup source relations
			foreach($relations['source'] AS $column => $relation){

				$parts = explode('_', $relation['table'], 2);
				$package = $parts[0];
				$model = isset($parts[1]) ? $parts[1] : $parts[0];
				$association_name = preg_replace('#_id$#','',$column);

				//If this association is already defined, move on.
				if(isset($associations[$association_name])) continue;

				$model_identifier = clone $identifier;
				$model_identifier->package = $package;
				$model_identifier->name = $model;

				//Include aliases file if it exists
				$alias = clone $identifier;
				$alias->package = $package;
				$this->includeAlias($alias);

				//Ensure the model & table exists
				try{
					$keys = array();
					if($table = $this->getService($model_identifier)->getTable()){

						$key = $table->mapColumns($relation['column'], true);
						$keys[$column] = $key;

						$associations[$association_name] = array('type' => 'one_one', 'model' => $model_identifier, 'keys' => $keys);
					}
				}catch(KException $e){}
			}

			//Setup target relations
			foreach($relations['target'] AS $column_id => $relationships){

				foreach($relationships AS $relation){

					try{
						$parts = explode('_', $relation['table'], 2);

						$package = $parts[0];
						$model = isset($parts[1]) ? $parts[1] : $parts[0];

						$model_identifier = clone $identifier;
						$model_identifier->package = $package;
						$model_identifier->name = $model;

						//Include aliases file if it exists
						$alias = clone $identifier;
						$alias->package = $package;
						$this->includeAlias($alias);

						//Get the table from the model
						if($table = $this->getService($model_identifier)->getTable()){

							//Get the related column from the realted table
							$related_column     = $table->getColumn($table->mapColumns($relation['column'], true));

							//Get the related tables relations
							$relation_relations = $innodb[$relation['table']];

							//Check for many_many relation
							//If the related table has uses at least 2 of the keys from the primary columns (through tables must use a composite key)
							//Check that all related columns are part of the key, if so, it's a many to many through relation
							$primary_key_columns = $related_column->related;
							$primary_key_columns[] = $related_column->name;
							$primary_key_relations = array_intersect($primary_key_columns, array_keys($relation_relations['source']));
							$related_columns_count = count($primary_key_relations);

							if($related_column->primary && $related_columns_count > 1){

								$through_identifier = clone $model_identifier;
								$source = $relation_relations['source'];

								//Remove keys that relate to the current table
								foreach($primary_key_relations AS $key){
									if($source[$key]['table'] == $table_name) unset($source[$key]);
								}

								//Should be left with 1 relation, from the through table to the target table
								if(count($source) != 1) continue;

								$relation_related_key = key($source);

								//Construct the model identifier for the target table
								$parts = explode('_', $source[$relation_related_key]['table'], 2);
								$package = $parts[0];
								$model = isset($parts[1]) ? $parts[1] : $parts[0];

								$model_identifier = clone $identifier;
								$model_identifier->package = $package;
								$model_identifier->name = $model;

								$association_name = $relation['table'];
								if(preg_match('#^'.preg_quote($table_name).'_#', $association_name)) $association_name = preg_replace('#^'.preg_quote($table_name).'_#', '', $association_name);
								else if (preg_match('#_'.preg_quote($table_name).'$#', $association_name)) $association_name = preg_replace('#_'.preg_quote($table_name).'$#', '', $association_name);

								$association_name = $this->getAlias($association_name);

								//If assocaition is already defined, move on
								if(isset($associations[$association_name])) continue;

								//Load the alias
								$this->includeAlias($model_identifier);

								//Get the relations related table and setup keys
								if($relation_related_table = $this->getService($model_identifier)->getTable()){
									$keys = array();
									$through_keys = array();

									foreach($primary_key_relations AS $col){
										$src = $relation_relations['source'][$col];
										if($src['table'] != $table_name){
											$keys[$key] = $relation_related_table->mapColumns($src['column'], true);
										}else{
											$through_keys[$this->_table->mapColumns($src['column'], true)] = $col;
										}
									}

									$keys = array($relation_related_key => $relation_related_table->mapColumns($source[$relation_related_key]['column'], true));
									$associations[$association_name] = array('type' => 'many_many', 'model' => $model_identifier, 'through' => $through_identifier, 'through_keys' => $through_keys, 'keys' => $keys);
								}
							}else{

								$column = $this->_table->mapColumns($column_id, true);

								$association_name = $related_column->unique ? KInflector::singularize($relation['table']) : KInflector::pluralize($relation['table']);
								$association_name = preg_replace('#^'.preg_quote($identifier->package).'_#','', $association_name);
								$association_name = $this->getAlias($association_name);

								//If association is already defined, move on
								if(isset($associations[$association_name])) continue;
								$associations[$association_name] = array('type' => $related_column->unique ? 'one_one' : 'one_many', 'model' => $model_identifier, 'keys' => array($column => $relation['column']));
							}
						}
					}catch(KException $e){}
				}
			}
		}

		return $associations;
	}


	/**
	 * Detect on name based matched
	 **/
	protected function _detectAssociationsByName($associations = array())
	{
		$identifier         = clone $this->getMixer()->getIdentifier();
		$identifier->path   = array('model');
		$tables             = $this->getTables();
		$package            = $identifier->package;
		$name               = $this->_table->getName();
		$stub_singular      = KInflector::singularize($identifier->name);
		$stub_plural        = KInflector::pluralize($identifier->name);
		$primary_keys       = array();
		$columns            = $this->_table->getColumns();

		//1:1 Associations
		foreach($columns AS $id => $column)
		{
			//Find all columns ending _id but not the id column
			if(preg_match('#_id$#', $column->name) && !$column->primary)
			{
				$column_name = $column->name;

				//Remove _id for the property name
				$association_id = preg_replace('#_id$#','',$column_name);
				$association_name = KInflector::singularize($association_id);
				$association_name = $this->getAlias($association_name);

				//Handle parent_id columns, associate back to self
				if($column->name == 'parent_id') $association_id = $stub_singular;

				//If this association is already defined, move on.
				if(isset($associations[$association_id])) continue;

				//Create table name
				$model_name = KInflector::pluralize($association_id);

				//Ensure the table actually exists
				if(isset($tables[$package.'_'.$model_name]))
				{
					$id = clone $identifier;
					$id->path = array('database','table');
					$id->name = $model_name;

					//Ensure the model & table exists
					try{
						$keys = array();
						$table = $this->getService($id);
						$pks = $table->getPrimaryKey();

						//Multi column primary keys do not work with 1:1 associations
						if(count($pks) > 1) continue;

						$keys[$column->name] = key($pks);

						$id = clone $id;
						$id->path = 'model';
						$associations[$association_name] = array('type' => 'one_one', 'model' => $id, 'keys' => $keys);

						//Setup children relation
						if($column->name == 'parent_id'){
							$associations['children'] = array('type' => 'one_many', 'model' => $id, 'keys' => array_flip($keys));
						}

					}catch(KException $e){

					}
				}
			}elseif($column->primary)
			{
				$primary_keys[$id] = $column->name;
			}
		}

		//1:N and N:N Associations
		foreach($tables AS $table)
		{
			//Ensure the table is part of this package
			if(preg_match('#^'.$package.'_#', $table) && $table != $name)
			{
				$table          = preg_replace('#^'.$package.'_#', '', $table);
				$is_one_many    = false;
				$is_many_many   = (bool) (preg_match('#^'.preg_quote($stub_plural).'_#', $table) || preg_match('#_'.preg_quote($stub_plural).'$#', $table));

				if(!$is_many_many){
					try{
						//Get DB table and columns
						$id = clone $this->_table->getIdentifier();
						$id->path = array('database','table');
						$id->name = $table;

						$dbtable    = $this->getService($id);
						$columns    = array_keys($dbtable->getColumns());

						//Ensure all this tables primary keys exist within the "associated" table
						if(!array_intersect($primary_keys, $columns) == $primary_keys) continue;

						//Detect relationship
						$is_many_many   = (bool) preg_match('#^'.preg_quote($stub_plural).'_#', $table) || preg_match('#_'.preg_quote($stub_plural).'$#', $table);
						$is_one_many    = true;

					}catch(Exception $e){}
				}


				//Validate 1:N association
				if($is_one_many)
				{
					$association_name = preg_replace('#^'.$stub_singular.'_#','', $table);
					$association_name = $this->getAlias($association_name);

					//If this association is already defined, move on.
					if(isset($associations[$association_name])) continue;

					//Construct the model identifier
					$id = clone $identifier;
					$id->name = $table;
					$associations[$association_name] = array('type' => 'one_many', 'model' => $id, 'keys' => $primary_keys);
				}
				//Validate N:N association
				elseif($is_many_many)
				{
					$association_name = preg_match('#^'.$stub_plural.'_#',$table) ? preg_replace('#^'.$stub_plural.'_#','', $table) : preg_replace('#_'.$stub_plural.'$#','', $table);

					//If this association is already defined, move on.
					if(isset($associations[$association_name])) continue;

					//Construct the model identifier
					$model_identifier = clone $identifier;
					$model_identifier->name = $association_name;

					//Construct the association model identifier
					$through_identifier = clone $identifier;
					$through_identifier->name = $table;
					$associations[$this->getAlias($association_name)] = array('type' => 'many_many', 'model' => $model_identifier, 'keys' => array(KInflector::singularize($association_name).'_id' => 'id'), 'through_keys' => $primary_keys, 'through' => $through_identifier);
				}
			}
		}

		return $associations;
	}


	/**
	 * Checks $name against the alias map
	 * @param $name
	 * @return mixed
	 */
	protected function getAlias($name)
	{
		if(isset($this->_associations_aliases[$name])) return $this->_associations_aliases[$name];
		else return $name;
	}


	/**
	 * Retrieves all the tables from the DB, strips off the table prefix and returns
	 * @return array
	 */
	protected function getTables()
	{
		//Check if tables have been cached previously
		if(!isset(self::$_tables))
		{
			if(extension_loaded('apc')){
				if(false !== $tables = apc_fetch('koowa-cache-identifier-tables')){
					self::$_tables = $tables;
					return self::$_tables;
				}
			}

			//Get the tables from the DB
			if(version_compare(Koowa::VERSION, '12.2', 'le')){
				//< 12.3 compat
				$query = 'SHOW TABLES';
			}else{
				$query = $this->getService('koowa:database.query.show')
					->show('TABLES');
			}

			$dbtables = $this->_table->getDatabase()->select($query, KDatabase::FETCH_FIELD_LIST);
			$prefix = $this->_table->getDatabase()->getTablePrefix();
			self::$_tables = array();
			foreach($dbtables AS $table)
			{
				if(preg_match('#^'.preg_quote($prefix).'#',$table))
				{
					$table = preg_replace('#^'.preg_quote($prefix).'#','', $table);
					self::$_tables[$table] = $table;
				}
			}

			if(extension_loaded('apc')){
				apc_store('koowa-cache-identifier-tables', self::$_tables);
			}
		}

		return self::$_tables;
	}



	/***
	 * Gets the InnoDB Table relations
	 * @return array
	 */
	protected function getTableRelations()
	{
		if(!isset(self::$_table_relations)){
			if(extension_loaded('apc')){
				if(false !== $relations = apc_fetch('koowa-cache-identifier-table-relations')){
					self::$_table_relations = $relations;
					return self::$_table_relations;
				}
			}

			$database = $this->_table->getDatabase();
			$db = $database->getDatabase();
			$prefix = preg_quote($database->getTablePrefix());
			$database->execute("use INFORMATION_SCHEMA", KDatabase::RESULT_USE);
			$result = $database->execute("SELECT TABLE_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM KEY_COLUMN_USAGE WHERE TABLE_SCHEMA='$db'", KDatabase::RESULT_USE);

			$relations = array();
			while ($row = $result->fetch_object())
			{
				$reference_table = preg_replace('#^'.$prefix.'#','', $row->REFERENCED_TABLE_NAME);
				$table =  preg_replace('#^'.$prefix.'#','', $row->TABLE_NAME);

				if($reference_table){

					if(!isset($relations[$table])) $relations[$table] = array('source' => array(), 'target' => array());
					$relations[$table]['source'][$row->COLUMN_NAME] = array('table' => $reference_table, 'column' => $row->REFERENCED_COLUMN_NAME);

					if(!isset($relations[$reference_table])) $relations[$reference_table] = array('source' => array(), 'target' => array());
					if(!isset($relations[$reference_table]['target'][$row->REFERENCED_COLUMN_NAME])) $relations[$reference_table]['target'][$row->REFERENCED_COLUMN_NAME] = array();
					$relations[$reference_table]['target'][$row->REFERENCED_COLUMN_NAME][] = array('table' => $table, 'column' => $row->COLUMN_NAME);
				}
			}

			$result->free();

			//Reset DB back to original
			$database->execute("use `$db`", KDatabase::RESULT_USE);

			self::$_table_relations = $relations;

			if(extension_loaded('apc')){
				apc_store('koowa-cache-identifier-table-relations', self::$_tables);
			}
		}

		return self::$_table_relations;
	}


	/**
	 * Merges many to many associations keys into the mixer
	 *
	 */
	protected function setManyToManyData()
	{
		$associations = $this->getAssociations('many_many');
		if(count($associations)){
			$mixer = $this->getMixer();
			$data = $mixer->getData();

			foreach($associations AS $property => $association){
				if($tmp = $this->getManyToManyAssociated($property, array(), 'keys')) {
					$data[$property] = isset($tmp['id']) ? $tmp['id'] : array();
				}
			}

			$mixer->setData($data, false);
		}
		return $this;
	}


	/**
	 * Chceks for a association for the passed property
	 * @param $property
	 * @return bool
	 */
	public function hasAssociation($property)
	{
		return isset($this->_associations) && isset($this->_associations[$property]);
	}


	/**
	 * Return the association for the given key if it exists
	 * @param $property
	 * @return KConfig|null
	 */
	public function getAssociation($property)
	{
		//Validate first
		if(!$this->hasAssociation($property)) return null;

		//Get the association
		if(!$association = $this->_associations[$property]){
			throw new KDatabaseBehaviorException('There is no association by the name ('.$property.') on '.$this->getMixer()->getIdentifier());
		}

		//Attempt to find association
		if(	!isset($association['type']) ||
			!isset($association['model']) ||
			!isset($association['keys']) ||
			!in_array($association['type'], array('one_one','one_many','many_many')))
		{
			throw new KDatabaseBehaviorException('The association ('.$property.') defined does not match the required format on '.$this->getMixer()->getIdentifier());
		}

		//Ensure the model is an identifier, if not, convert to ID
		if(strpos((string) $association['model'],'.') === false)
		{
			$identifier = clone $this->getMixer()->getIdentifier();
			$identifier->path = array('model');
			$identifier->name = $association['model'];
			$association['model'] = $identifier;
		}


		//Ensure the through model is an identifier, if not, convert to ID
		if(isset($association['through']) && strpos((string) $association['through'],'.') === false)
		{
			$identifier = clone $this->getMixer()->getIdentifier();
			$identifier->path = array('model');
			$identifier->name = $association['through'];
			$association['through'] = $identifier;
		}

		$this->_associations[$property] = $association;

		return new KConfig($association);
	}


	/**
	 * Returns the defined associations
	 * @return array
	 */
	public function getAssociations($type = null)
	{
		if(isset($this->_associations)){

			if($type){
				$associations = array();
				foreach($this->_associations AS $id => $association){
					if($association['type'] == $type) $associations[$id] = $association;
				}

				return $associations;
			}else{
				return $this->_associations;
			}
		}else{
			return array();
		}
	}


	/**
	 * Sets an association for a given property
	 * @param $property
	 * @param array $association
	 * @return KDatabaseBehaviorAssociatable
	 * @throws KDatabaseRowException
	 */
	public function setAssociation($property, array $association)
	{
		//Validate association
		if(	!isset($association['type']) ||
			!isset($association['model']) ||
			!isset($association['keys']) ||
			!in_array($association['type'], array('one_one','one_many','many_many')))
		{
			throw new KDatabaseRowException(__CLASS__.'::'.__FUNCTION__.' association requires a type, model and keys property');
		}

		//Ensure many to many relationships have a through model
		if($association['type'] == 'many_many' && (!isset($association['through']) || !isset($association['through_keys'])))
		{
			throw new KDatabaseRowException(__CLASS__.'::'.__FUNCTION__.' many to many associations require a "through" model and "through_keys"');
		}

		//Ensure the model is an identifier, if not, convert to ID
		if(strpos((string) $association['model'],'.') === false)
		{
			$identifier = clone $this->getMixer()->getIdentifier();
			$identifier->path = array('model');
			$identifier->name = $association['model'];
			$association['model'] = $identifier;
		}

		//Set the association
		$property = $this->getAlias($property);
		$this->_associations[$property] = $association;

		return $this;
	}


	/**
	 * Gets all data for all associations
	 * @param bool $modified
	 * @return array
	 */
	public function getAssociatedData($maxDepth = 2, $depth = 0)
	{
		static $requested = array();

		//Construct an identifier to Check if this has been requested before, if so don't request again
		$mixer              = $this->getMixer();
		$id                 = (string) $mixer->getIdentifier();
		$identity_column    = $mixer->getIdentityColumn();

		if($identity_column)
		{
			$id .= '.'.$mixer->get($identity_column);
		}else{
			$cols = $mixer->getTable()->getPrimaryKey();
			foreach($cols AS $col_id => $column) $id .= '.'.$mixer->get($col_id);
		}

		//Ensure previously requested items do not get re-requested, endless loop hazard
		if(isset($requested[$id])) return null;
		$requested[$id] = true;

		//Get mixer data first
		$data = $mixer->getData();
		if(isset($data['associations'])) unset($data['associations']);

		//Ensure we're not exceeding depth
		if($maxDepth > $depth)
		{
			$depth++; //Increment depth
			foreach(array_keys($this->getAssociations()) AS $property)
			{
				$associated = $mixer->getAssociated($property);

				//If the associated item is a rowset, iterate and request data
				if($associated instanceof KDatabaseRowsetAbstract)
				{
					$associated_data = array();
					$associatable = $associated->isAssociatable();
					foreach($associated AS $id => $row)
					{
						if($associatable){
							$associated_data[$id] = $row->getAssociatedData($maxDepth, $depth);
						}else{
							$associated_data[$id] = $row->getData();
						}

						//If data returned is null (depth > maxDepth) unset
						if(is_null($associated_data[$id])) unset($associated_data[$id]);
					}

					if(!empty($associated_data)) $data[$property] = $associated_data;

				}else if($associated instanceof KDatabaseRowAbstract){
					if($associated->isAssociatable())
					{
						$data[$property] = $associated->getAssociatedData($maxDepth, $depth);
					}else{
						$data[$property] = $associated->getData();
					}

					//If data returned is null (depth > maxDepth) unset
					if(is_null($data[$property])) unset($data[$property]);
				}
			}
		}

		//Reset the requested array
		if($depth == 1) $requested = array();

		return $data;
	}


	/**
	 * Retrieves the associated data for a property
	 * @param $property
	 * @param array $args
	 * @param null $type
	 * @return KDatabaseRow|KDatabaseRowSet|null
	 */
	public function getAssociated($property, $state = array())
	{
		//Attempt to find association
		if(!($association = $this->getAssociation($property)))
		{
			throw new KDatabaseBehaviorException('No association defined for ('.$property.') in ('.$this->getMixer()->getIdentifier().')');
		}

		//Return data for the relevant type
		switch($association->type)
		{
			case 'one_one':
				return $this->getOneToOneAssociated($property);
				break;

			case 'one_many':
				return $this->getOneToManyAssociated($property, $state);
				break;

			case 'many_many':
				return $this->getManyToManyAssociated($property, $state);
				break;
		}

		return null;
	}


	/**
	 * Collects data for a one to one association for this record
	 * @param string $property
	 * @return KDatabaseRow|null
	 */
	public function getOneToOneAssociated($property)
	{
        //Load from cache
        $hash = spl_object_hash($this->getMixer());
        if(isset($this->_associated[$hash][$property])) return $this->_associated[$hash][$property];

		//Ensure associated exists and is complete
		if(!($association = $this->getAssociation($property)))
		{
			throw new KDatabaseBehaviorException('No association defined for ('.$property.') in ('.$this->getMixer()->getIdentifier().')');
		}

		//Include alias
		$this->includeAlias($association->model);

		//Check the identifier is a model
		$model = $this->getService($association->model);
		if(!$model instanceof KModelAbstract){
			throw new KDatabaseBehaviorException('Model for ('.$property.') is not an instance of KModelAbstract ('.$association->model.')');
		}

		//Get the mixer for the data
		$mixer = $this->getMixer();

		//Map the states foreign keys
		$state = array();
		foreach($association->keys->toArray() AS $lk => $fk)
		{
			if(!isset($state[$fk]))	$state[$fk] = $mixer->$lk;
		}

		//Can't have empty state
		if(empty($state)){
			return $model->getTable()->getRow();
		}

		$this->_associated[$hash][$property] = $model->set($state)->getItem();
        return $this->_associated[$hash][$property];
	}


	/**
	 * Collects data for a one to many association for this record
	 * @param string $property
	 * @param array $state - Optional state data
	 * @return KDatabaseRowSet|null
	 */
	public function getOneToManyAssociated($property, $state = array())
	{
        //Load from cache
        $hash = spl_object_hash($this->getMixer());
        if(isset($this->_associated[$hash][$property])) return $this->_associated[$hash][$property];

		//Ensure associated exists and is complete
		if(!($association = $this->getAssociation($property)))
		{
			throw new KDatabaseBehaviorException('No association defined for ('.$property.') in ('.$this->getMixer()->getIdentifier().')');
		}

		//Include alias
		$this->includeAlias($association->model);

		//Check the identifier is a model
		$model = $this->getService($association->model);
		if(!$model instanceof KModelAbstract){
			throw new KDatabaseBehaviorException('Model for ('.$property.') is not an instance of KModelAbstract ('.$association->model.')');
		}

		//Get the mixer for the data
		$mixer = $this->getMixer();

		//Map the states foreign keys
		foreach($association->keys->toArray() AS $lk => $fk)
		{
			if(!isset($state[$fk]) && $mixer->$lk !== null) $state[$fk] = $mixer->$lk;
		}

		//Ensure we have a key
		if(empty($state)){
			return $model->getTable()->getRowset();
		}

		//Get the models states
		$modelState = $model->getState()->toArray();

		//Ensure the state
		if(array_intersect_key($state, $modelState) != $state)
		{
			throw new KDatabaseBehaviorException('The model '.$model->getIdentifier().' must contain the following states for the associatable behavior to work: '.implode(',',array_keys($state)));
		}

		//Ensure that each key is a state in the relevant model
		$this->_associated[$hash][$property] = $model->set($state)->getList();
        return $this->_associated[$hash][$property];
	}


	/**
	 * Collects data for a many to many association with this record
	 * @param string $property
	 * @param array $state - Optional state data
	 * @return KDatabaseRow|null
	 */
	public function getManyToManyAssociated($property, $state = array(), $return = false)
	{
        //Load from cache
        $hash = spl_object_hash($this->getMixer());
        if(isset($this->_associated[$hash][$property])) return $this->_associated[$hash][$property];

		//Ensure associated exists and is complete
		if(!($association = $this->getAssociation($property)))
		{
			throw new KDatabaseBehaviorException('No association defined for ('.$property.') in ('.$this->getMixer()->getIdentifier().')');
		}

		//Include alias
		$this->includeAlias($association->model);

		//Check the identifier is a model
		$model = $this->getService($association->model);
		if(!$model instanceof KModelAbstract){
			throw new KDatabaseBehaviorException('Model for ('.$property.') is not an instance of KModelAbstract ('.$association->model.')');
		}

		//Get the mixer for the data
		$mixer = $this->getMixer();

		//Include alias
		$this->includeAlias($association->through);

		//Check for a through model
		$associated_model = $this->getService($association->through);
		if(!$model instanceof KModelAbstract){
			throw new KDatabaseBehaviorException('Through model for ('.$property.') is not an instance of KModelAbstract ('.$association->through.')');
		}

		//Map the states foreign keys
		$through_state = array();
		foreach($association->through_keys->toArray() AS $lk => $fk)
		{
			if(!isset($through_state[$fk]) && $mixer->get($lk) !== null) $through_state[$fk] = $mixer->get($lk);
		}

		//Ensure we have a key
		if(empty($through_state)){
			return $return == 'keys' ? array() : $model->getTable()->getRowset();
		}

		//Get the models states
		$modelState = $associated_model->getState()->toArray();

		//Ensure the requested states exist
		if(array_intersect_key($through_state, $modelState) != $through_state)
		{
			throw new KDatabaseBehaviorException('The through model '.$model->getIdentifier().' must contain the following states for the associatable behavior to work: '.implode(',',array_keys($through_state)));
		}

		//Ensure that each key is a state in the relevant model
		$associations = $associated_model->set($through_state)->getList();

		//If no through data was found, return null
		if(!$associations->count()) {
			return $return == 'keys' ? array() : $model->getTable()->getRowset();
		}

		//Return the raw associations?
		if($return == 'associations') return $associations;

		//Check if the related mode uses the same table as the mixer, if so, we exclude the mixers id
		$self = $model->getTable() == $mixer->getTable();

		//Compile associated items state
		foreach($associations AS $associated)
		{
			foreach($association->keys AS $lk => $fk)
			{
				$value = $associated->get($lk);
				if(!isset($state[$fk])) $state[$fk] = array();
				if($value !== null && (!$self || ($self && $mixer->$fk != $value))) $state[$fk][] = $value;
			}
		}

		//Check if we only want the keys?
		if($return == 'keys'){
			return $state;
		}

		//Ensure we have state info
		if(empty($state)){
			return $model->getTable()->getRowset();
		}

		//Get the models states
		$modelState = $model->getState()->toArray();

		//Ensure the state
		if(array_intersect_key($state, $modelState) != $state)
		{
			throw new KDatabaseBehaviorException('The model '.$model->getIdentifier().' must contain the following states for the associatable behavior to work: '.implode(',',array_keys($state)));
		}

		//Get the associated items
		$this->_associated[$hash][$property] = $model->set($state)->getList();
        return $this->_associated[$hash][$property];
	}



	/**
	 * Includes an alias file
	 * @TODO: Remove: This should really be included by the framework
	 * @param $identifier
	 * @return KBehaviorAssociatable
	 */
	protected function includeAlias($identifier)
	{
		//Include alias
		$alias = clone $this->getIdentifier($identifier);
		$alias->path = array();
		$alias->name = 'aliases';
		if(file_exists($alias->filepath)) require_once $alias->filepath;

		return $this;
	}


	/**
	 * Allows for get$property aliased methods to retrieve associated data by an aliased get method.
	 * eg getItems => $items
	 * @param $method
	 * @param $arguments
	 * @return KDatabaseRow|KDatabaseRowSet|mixed|null
	 */
	public function __call($method, $arguments)
	{
		if($method !== 'mixin'){
			$state = isset($arguments[0]) ? $arguments[0] : array();
			if($this->hasAssociation($method)) return $this->getAssociated($method, $state);

			if(preg_match('#^get[A-Z]#', $method)){
				$property = lcfirst(preg_replace('#^get#', '', $method));
				if($this->hasAssociation($property)) return $this->getAssociated($property, $state);
			}
		}

		return parent::__call($method, $arguments);
	}
}